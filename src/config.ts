export const secret = {
  ACCESS_TOKEN_SECRET:
    'f6734df8ccecaf43ed99afa33e04f1e206b2e91b915b35583ceba003215f558114a448678ca430831caca4222ce81a6a5900dba326a163a8e017c41252563940',
  REFRESH_TOKEN_SECRET:
    '795d1187c31f811ab0a61104cc1c63b7a873b2e120827378cc44b2013aa5bbac5f90bf0ea30a91ae8748d4df99b4f7a5ba55421c0190e037dfc90218708f74f3',
};
export const db = {
  host: 'localhost',
  port: '3000',
  name: 'db',
  string: 'mongodb://localhost:27017/coding-sans-backend',
};
