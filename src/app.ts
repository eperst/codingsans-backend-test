import express, { Request, Response, Application, NextFunction } from 'express';
import { secret, db } from './config';
import { User, UserClass } from './db';
import connect from './dbconnect';
import { AxiosResponse } from 'axios';
const bcrypt = require('bcrypt').default; //for safe password storage
const axios = require('axios').default;
const app: Application = express();
const jwt = require('jsonwebtoken').default;
import * as bodyParser from 'body-parser';

app.use(bodyParser.json());
app.use(express.json({ limit: '50mb' }));
app.post('/users', async (req: Request, res: Response) => {
  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const user = new User({ id: req.body.id, username: req.body.username, password: hashedPassword });
    res.send(user);
  } catch {
    res.send('Error occured');
  }
});

app.post('/login', async (req: Request, res: Response) => {
  await User.find({ username: req.body.username })
    .limit(1)
    .exec()
    .then(async (result: UserClass[]) => {
      if (result.length === 0) {
        return res.status(400).send('Cannot find user');
      }
      try {
        if (await bcrypt.compare(req.body.password, result[0].password)) {
          const token: string = generateAccessToken(result[0]);
          const refreshToken: string = generateRefreshToken(result[0]);
          return res.json({ accessToken: token, refreshToken: refreshToken });
        } else {
          return res.status(401).send('Wrong password');
        }
      } catch (err) {
        return res.send('error occured');
      }
    });
});

function generateAccessToken(user: UserClass): string {
  return jwt.sign({ username: user.username, password: user.password }, secret.ACCESS_TOKEN_SECRET, {
    expiresIn: '1h',
  });
}

function generateRefreshToken(user: UserClass): string {
  return jwt.sign({ username: user.username, password: user.password }, secret.REFRESH_TOKEN_SECRET);
}

app.get('/breweries', authenticateToken, async (req: Request, res: Response) => {
  await query(req.body.query).then((data) => {
    console.log(data);
    res.send(data); //circular structure error
  });
});

async function query(parameter: string): Promise<Response> {
  try {
    if (parameter === null) {
      return axios
        .request({
          url: 'https://api.openbrewerydb.org/breweries',
          transformresponse: (r: AxiosResponse) => r.data,
        })
        .then((response: AxiosResponse) => {
          const { data } = response;
          return data;
        });
    }
    return await axios.get('https://api.openbrewerydb.org/breweries', {
      params: {
        query: parameter,
      },
    });
  } catch (err) {
    return err;
  }
}

function authenticateToken(req: Request, res: Response, next: NextFunction): string {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1]; //returns token or undefined
  if (token === null) {
    res.sendStatus(401);
    return '';
  } //no token
  jwt.verify(token, secret.ACCESS_TOKEN_SECRET, (err: ErrorEvent) => {
    if (err) {
      return res.sendStatus(403);
    } //invalid token
    return token;
  });
  next();
  return '';
}

export const startApp = (): Promise<void> => {
  return new Promise<void>((resolve, reject) => {
    if (connect(db.string)) {
      resolve();
    } else {
      reject();
    }
  }).then(
    () => {
      app.listen(db.port, () => {
        console.log(`Server running on port ${db.port}`);
      });
    },
    (err: ErrorEvent) => {
      console.log(err);
      process.exit(1);
    },
  );
};
