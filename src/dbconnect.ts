import mongoose from 'mongoose';

export default (db: string): boolean => {
  const connect = (): boolean => {
    mongoose
      .connect(db, { useUnifiedTopology: true, useNewUrlParser: true })
      .then((): boolean => {
        console.log(`Successfully connected to ${db}`);
        return true;
      })
      .catch((error: ErrorEvent): boolean => {
        console.log('Error connecting to database: ', error);
        return false;
      });
    return false;
  };
  connect();
  mongoose.connection.on('disconnected', connect);
  return true;
};
