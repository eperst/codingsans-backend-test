import mongoose, { Schema, Document } from 'mongoose';

export interface UserClass extends Document {
  id: string;
  username: string;
  password: string;
}

const UserSchema: Schema = new Schema({
  id: { type: String, required: true },
  username: { type: String, required: true },
  password: { type: String, required: true },
});

export const User = mongoose.model<UserClass>('User', UserSchema);
